Your satisfaction with the hearing solution we recommend for you is of utmost importance to us. We know that the more you understand about your level of hearing loss and the solutions that are available to you, the greater the chances are that you will be completely satisfied with your investment.

Address: 225 Memorial Dr, #101, Berlin, WI 54923, USA

Phone: 920-969-1768
